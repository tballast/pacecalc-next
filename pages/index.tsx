import React from 'react'
import MainLayout from '../layouts/main-layout'
import RunningCalculator from '../components/running-calculator'
import { GetStaticProps } from 'next'
import deepmerge from 'deepmerge'
// TODO Figure out how to properly make this responsive.

// TODO Add a footer with Buy Me A Coffee Link!
// TODO Examime Mantine UI, Chakra UI, and Material UI
// TODO MAterial Example: https://github.com/mui/material-ui/tree/master/examples/nextjs-with-typescript
// TODO Chakra Example: https://chakra-ui.com/getting-started/nextjs-guide
// TODO Mantine Example: https://mantine.dev/guides/next/
// https://www.buymeacoffee.com/tylerballast
export default function Home() {
  //tools.wordstream.com/fkt?website=pace+calculator&cid=&camplink=&campname=  https:
  return (
    <MainLayout>
      <RunningCalculator />
    </MainLayout>
  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  // I you don' use internationalized routing, define this statically.
  const locale = context.locale

  const userMessages = (await import(`../messages/${locale}.json`)).default
  const defaultMessages = (await import(`../messages/en.json`)).default
  const messages = deepmerge(defaultMessages, userMessages)

  return {
    props: {
      messages,
    },
  }
}
