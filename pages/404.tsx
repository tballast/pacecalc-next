import Link from 'next/link'
import prefill from '../utils/canvas-404-prefill'
import React, {
  MouseEvent,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react'
import { twMerge as tw } from 'tailwind-merge'
import {
  IconDownload,
  IconWriting,
  IconEraser,
  IconClearAll,
} from '@tabler/icons'

type Coordinate = {
  x: number
  y: number
}

const Button = (props: any) => {
  const { children, className, bgColor, ...other } = props
  return (
    <button
      {...other}
      className={tw(
        `click:bg-blue-800 rounded bg-blue-500 py-2 
        px-4 font-bold text-white 
      hover:bg-blue-700`,
        className
      )}
    >
      {children}
    </button>
  )
}

// https://www.ankursheel.com/blog/react-component-draw-page-hooks-typescript
const FourOhFour = () => {
  const ref = useRef<HTMLCanvasElement>(null)
  const [mousePos, setMousePos] = useState<Coordinate | undefined>(undefined)
  const [isPainting, setIsPainting] = useState(false)
  const [mode, setMode] = useState<'draw' | 'erase'>('draw')
  const [strokeStyle, setStrokeStyle] = useState('#A81815')
  const [lineWidth, setLineWidth] = useState(5)
  const [wh, setWh] = useState({ width: 200, height: 100 })
  const [isLoaded, setIsLoaded] = useState(false)

  const getCoordinates = (event: MouseEvent): Coordinate | undefined => {
    if (!ref.current) {
      console.log('No Canvas')
      return
    }
    const canvas: HTMLCanvasElement = ref.current
    // console.log(canvas)
    // console.log(event)

    return {
      x: event.clientX - canvas.offsetLeft,
      y: event.clientY - canvas.offsetTop,
    }
  }

  const startPaint = useCallback((e: MouseEvent) => {
    const p = getCoordinates(e)
    if (e) {
      // Set Coordinates
      setMousePos(p)
      // Start Painting
      setIsPainting(true)
    }
  }, [])

  const stopPaint = useCallback(() => {
    // Stop Painting
    setIsPainting(false)
    setMousePos(undefined)
  }, [])

  const setDrawMode = () => {
    if (ref.current) {
      const context = ref.current.getContext('2d')
      if (context) {
        context.globalCompositeOperation = 'source-over'
        setMode('draw')
      }
    }
  }

  const setEraseMode = () => {
    if (ref.current) {
      const context = ref.current.getContext('2d')
      if (context) {
        context.globalCompositeOperation = 'destination-out'
        setMode('erase')
      }
    }
  }

  const toggleMode = () => {
    if (mode === 'draw') setEraseMode()
    else setDrawMode()
  }

  // https://stackoverflow.com/questions/7140531/how-to-make-a-scalable-custom-cursor-for-html5-canvas-drawing
  const draw = (oldPos: Coordinate, newPos: Coordinate) => {
    if (ref.current) {
      const context = ref.current.getContext('2d')
      if (context && oldPos && newPos) {
        context.strokeStyle = strokeStyle
        context.lineJoin = 'round'
        context.lineWidth = lineWidth

        context.beginPath()
        context.moveTo(oldPos.x, oldPos.y)
        context.lineTo(newPos.x, newPos.y)
        context.closePath()

        context.stroke()
      }
    }
  }

  const paint = useCallback(
    (e: MouseEvent) => {
      if (isPainting) {
        const newMousePosition = getCoordinates(e)
        console.log(newMousePosition)
        if (newMousePosition && mousePos) {
          draw(mousePos, newMousePosition)
          setMousePos(newMousePosition)
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isPainting, mousePos]
  )

  const downloadCanvas = () => {
    let downloadLink = document.createElement('a')
    downloadLink.setAttribute('download', 'pace-calc-image.png')
    // let canvas = document.getElementById('canvas404')
    const canvas = ref.current
    if (canvas) {
      canvas.toBlob(function (blob) {
        if (blob) {
          let url = URL.createObjectURL(blob)
          downloadLink.setAttribute('href', url)
          downloadLink.click()
        }
      })
    }
  }

  // #04DAE3
  // #7D04E3
  // #E30D04
  // #6AE304

  // #31CE9E
  // #5031CE
  // #CE3161
  // #AFCE31

  const clearCanvas = () => {
    const canvas = ref.current
    if (canvas) {
      const context = canvas.getContext('2d')
      if (context) context.clearRect(0, 0, canvas.width, canvas.height)
    }
  }

  const onMouseDown = (e: MouseEvent) => {
    startPaint(e)
  }
  const onMouseUp = (e: MouseEvent) => {
    stopPaint()
  }

  const onMouseMove = (e: MouseEvent) => {
    paint(e)
  }

  useEffect(() => {
    if (ref.current && isLoaded) {
      console.log('prefilling')
      prefill(
        ref.current.getContext('2d'),
        ref.current.clientHeight,
        ref.current.clientWidth
      )
    }
  }, [isLoaded])

  // const handleResize = React.useCallback(() => {
  //   if (ref.current) {
  //     const width = ref.current?.clientWidth || 200
  //     const height = ref.current?.clientHeight || 100
  //     console.log(ref.current.clientWidth)
  //     setWh({ width: width, height: height })
  //     setIsLoaded(true)
  //   }
  // }, [ref.current])

  // This is causing the 404 to disappear.
  useEffect(() => {
    // const handleResize = () => {
    //   if (ref.current) {
    //     const width = ref.current?.clientWidth || 200
    //     const height = ref.current?.clientHeight || 100
    //     setWh({ width: width, height: height })
    //   }
    // }

    setIsLoaded(true)
    // window.addEventListener('resize', handleResize)

    // return () => {
    //   window.removeEventListener('resize', handleResize)
    // }
  }, [])

  // Colour Picke
  // https://allenhwkim.medium.com/3-steps-to-build-a-color-picker-4badd1e96854
  // Mouse
  // https://blog.logrocket.com/creating-custom-mouse-cursor-css/

  const colours = [
    { hex: '#A81815', class: 'bg-[#A81815]' },
    { hex: '#31CE9E', class: 'bg-[#31CE9E]' },
    { hex: '#5031CE', class: 'bg-[#5031CE]' },
    { hex: '#AFCE31', class: 'bg-[#AFCE31]' },
  ]

  return (
    <div className='container mx-auto flex min-h-screen flex-col'>
      <div className='mt-6 flex h-full flex-col md:grid md:grid-cols-12'>
        <div className='flex flex-row justify-self-end md:col-span-1 md:col-start-2 md:flex-col'>
          <Button
            className='w-fit max-sm:m-2 md:mb-2 md:ml-2 md:mr-2'
            onClick={downloadCanvas}
            title='Download Canvas as PNG'
          >
            <IconDownload />
          </Button>
          <Button
            className='m-2 w-fit'
            onClick={clearCanvas}
            title='Clear Canvas'
          >
            <IconClearAll />
          </Button>
          <Button
            className='m-2 w-fit'
            onClick={toggleMode}
            title={mode === 'draw' ? 'Turn on Erase Mode' : 'Turn on draw mode'}
          >
            {mode === 'draw' ? <IconEraser /> : <IconWriting />}
          </Button>
        </div>
        <canvas
          // style={{ cursor: 'url(/cursor/circle-5px-opt.svg) 3 3,pointer' }}
          className='col-span-8 col-start-3 rounded border bg-teal-100
          [cursor:url(/cursor/circle-5px-opt.svg)_3_3,pointer] hover:border-sky-300 max-sm:mx-2
           md:h-full md:w-full 
         '
          ref={ref}
          height={ref.current?.clientHeight || 100}
          width={ref.current?.clientWidth || 200}
          // height={wh.height}
          // width={wh.width}
          onMouseDown={onMouseDown}
          onMouseMove={onMouseMove}
          onMouseUp={onMouseUp}
          onMouseOut={onMouseUp}
        ></canvas>
        <div className='flex flex-row justify-self-start md:col-span-1 md:col-start-11 md:flex-col '>
          {colours.map((colour) => (
            <Button
              key={colour.hex}
              // bgColor={colour}
              className={`w-fit ${colour.class} hover:opacity-30 max-sm:m-2 md:mb-2 md:ml-2 md:mr-2`}
              onClick={() => setStrokeStyle(colour.hex)}
              title={`Set color to ${colour.hex} `}
            >
              {colour.hex}
            </Button>
          ))}
        </div>
      </div>
      <Link
        href={'/'}
        className='m-2 w-auto self-center rounded border border-sky-300 bg-sky-600 p-2 text-4xl font-bold text-gray-100 shadow-md hover:text-cyan-700 hover:underline dark:hover:text-cyan-100'
      >
        Home
      </Link>
      <h1 className='mt-auto mb-4 max-h-11 self-center text-6xl font-bold text-sky-300 text-opacity-70'>
        404
      </h1>
    </div>
  )
}
export default FourOhFour
