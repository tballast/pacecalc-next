import type { AppProps } from 'next/app'

import '../styles/vars.css'
import '../styles/global.css'
// import './app.css'
import Script from 'next/script'
import { NextAdapter } from 'next-query-params'
import { QueryParamProvider } from 'use-query-params'
import * as gtag from '../lib/ga/gtag'
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import { GA_TRACKING_ID } from '../lib/ga/gtag-tracking-id'
import React from 'react'
import { ThemeProvider } from 'next-themes'
import { NextIntlProvider } from 'next-intl'

// Custom Font Imports
import '@fontsource/azeret-mono'
// import '@fontsource/ibm-plex-mono'
// import '@fontsource/anonymous-pro'
// import '@fontsource/dm-sans'
import { Outfit } from '@next/font/google'

const ptsans = Outfit({
  subsets: ['latin'],
  variable: '--font-ptsans',
  // weight: ['400', '700'],
})

// This file allows us to inject elements that will be common to all pages.
// when we route to something, this will ultimately be passed to this component as "Component"
// And we can then alter things like styling, routing, etc.

/*
Motion Library: https://www.framer.com/motion/  
Headless UI
  Daisy UI
  Tailwind UI
  tailwind-components
  https://tailwind-elements.com/docs/standard/forms/inputs/
  React Aria : https://react-spectrum.adobe.com/react-aria/useTabList.html
  Radix-UI https://www.radix-ui.com/docs/primitives/overview/getting-started
  Reach UI https://reach.tech/tabs
  Comparison: https://backlight.dev/mastery/best-react-component-libraries-for-design-systems
  */

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter()
  useEffect(() => {
    const handleRouteChange = (url: any) => {
      gtag.pageview(url)
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  return (
    <>
      {/* Global Site Tag (gtag.js) - Google Analytics */}
      <Script
        strategy='afterInteractive'
        src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
      />
      <Script
        id={'2'}
        strategy='afterInteractive'
        dangerouslySetInnerHTML={{
          __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${GA_TRACKING_ID}', {
              page_path: window.location.pathname,
            });
          `,
        }}
      />
      <NextIntlProvider messages={pageProps.messages}>
        <ThemeProvider attribute='class'>
          <QueryParamProvider adapter={NextAdapter}>
            <div className={`${ptsans.variable} font-sans`}>
              <Component {...pageProps} />
            </div>
          </QueryParamProvider>
        </ThemeProvider>
      </NextIntlProvider>
    </>
  )
}

export default MyApp
