import { Head, Html, Main, NextScript } from 'next/document'

// export default class _Document extends Document {
export default function Document() {
  return (
    <Html>
      <Head />
      <body className=' bg-gradient-to-tr from-sky-200 to-cyan-200 dark:from-slate-800 dark:to-gray-800'>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
