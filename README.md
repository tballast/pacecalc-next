# PaceCalc.run

A simple pace and finish time calculator for runners.

[Live](https://www.pacecalc.run/)

# Screenshots

![Screenshot](./pacecalc.png)

![Mobile Screenshot](./pacecalc-mobile.png)
