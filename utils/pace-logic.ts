import { UNITS_SPEED } from '../constants/speed'

/**
 * Minute.Second to Second
 * @param hours Number of Hours
 * @param minutes Number of Minutes
 * @param seconds Number of Seconds
 * @returns Total time in seconds
 */
export const hms2s = (
  hours: number = 0,
  minutes: number = 0,
  seconds: number = 0
) => hours * 60 * 60 + minutes * 60 + seconds

/**
 * Seconds to H:M:S
 * @param totalSeconds Number of Seconds
 * @returns [hours, minutes, seconds]
 */
export const s2hms = (totalSeconds: number = 0) => {
  const hours = Math.floor(totalSeconds / 3600)
  const rem = totalSeconds % 3600
  const minutes = Math.floor(rem / 60)
  const remm = rem % 60
  const seconds = remm
  return [hours, minutes, seconds]
}

/*
    Input Pace (m/s)
    Input Distance (m)

    Output: Duration
*/
export const calcDuration = (speed: number, distance: number) => {
  const duration = distance / speed
  return duration
}

/**
 * Input Duration (s)
 * Input Distance (m)
 *
 * Output: Speed (Unit)
 */
export const calcPace = (
  duration: number,
  distance: number,
  unit: UNITS_SPEED
) => {
  switch (unit) {
    case UNITS_SPEED.METERS_PER_SECOND:
      return distance / duration
    case UNITS_SPEED.KILOMETERS_PER_HOUR:
      return distance / 1000 / (duration / 3600)
    case UNITS_SPEED.MILES_PER_HOUR:
      return (distance / duration) * 2.237
    case UNITS_SPEED.MINUTES_PER_KILOMETER:
      // Convert time to Minutes, convert distance to KM
      return duration / 60 / (distance / 1000)
    case UNITS_SPEED.MINUTES_PER_MILE:
      // Convert time to Mminutes, convert Distance to Miles
      return duration / 60 / (distance / 1609)
    default: // km/h
      return distance / 1000 / (duration / 3600)
  }
}

export enum SELECT_OPTIONS {
  HHMM,
  MMSS,
  HHMMSS,
}

interface IFormatOptions {
  trimLeadingZeros?: boolean
  select?: SELECT_OPTIONS
}

// export const formatSeconds = (seconds: number, opts?: IFormatOptions) => {
//   const trim = (val: string) => {
//     return opts && opts.trimLeadingZeros ? val.replace(/^0+/, '') : val
//   }

//   let ans = null
//   console.log(seconds)
//   console.log(new Date(seconds * 1000).toISOString())
//   if (opts)
//     switch (opts.select) {
//       case SELECT_OPTIONS.HHMM:
//         ans = new Date(seconds * 1000).toISOString().substring(11, 16)
//         break
//       case SELECT_OPTIONS.MMSS:
//         ans = new Date(seconds * 1000).toISOString().substring(14, 19)
//         break
//       case SELECT_OPTIONS.HHMMSS: // Breakthrough
//         ans = new Date(seconds * 1000).toISOString().substring(11, 19)
//         break
//       default:
//         ans =
//           seconds > 3600
//             ? new Date(seconds * 1000).toISOString().substring(11, 19)
//             : new Date(seconds * 1000).toISOString().substring(14, 19)
//     }
//   // x = x.replace(/^0+/, '');
//   else {
//     ans =
//       seconds > 3600
//         ? new Date(seconds * 1000)
//             .toISOString()
//             .substring(11, 19)
//             .replace(/^0+/, '')
//         : new Date(seconds * 1000)
//             .toISOString()
//             .substring(14, 19)
//             .replace(/^0+/, '')
//   }

//   return trim(ans)
// }

// Borrowed from https://github.com/ungoldman/format-duration/blob/main/format-duration.js
function addZero(value: number, digits?: number) {
  digits = digits || 2

  let str = value.toString()
  let size = 0

  size = digits - str.length + 1
  str = new Array(size).join('0').concat(str)

  return str
}

const parseMs = (ms: number) => {
  if (typeof ms !== 'number') {
    throw new TypeError('Expected a number')
  }

  return {
    years: Math.trunc(ms / 31556926000),
    days: Math.trunc(ms / 86400000) % 365,
    hours: Math.trunc(ms / 3600000) % 24,
    minutes: Math.trunc(ms / 60000) % 60,
    seconds: Math.trunc(ms / 1000) % 60,
    ms: Math.trunc(ms) % 1000,
  }
}

export const formatSeconds = (ms: number, leadingZeros: boolean = true) => {
  const t = parseMs(ms * 1000)
  const l = leadingZeros

  if (t.years)
    return (
      t.years +
      'Y:' +
      t.days +
      'D:' +
      (l ? addZero(t.hours) : t.hours) +
      ':' +
      (l ? addZero(t.minutes) : t.minutes) +
      ':' +
      (l ? addZero(t.seconds) : t.seconds)
    )
  if (t.days)
    return (
      t.days +
      'D:' +
      (l ? addZero(t.hours) : t.hours) +
      ':' +
      (l ? addZero(t.minutes) : t.minutes) +
      ':' +
      (l ? addZero(t.seconds) : t.seconds)
    )

  if (t.hours)
    return (
      t.hours +
      ':' +
      (l ? addZero(t.minutes) : t.minutes) +
      ':' +
      (l ? addZero(t.seconds) : t.seconds)
    )

  return (
    (l ? addZero(t.minutes) : t.minutes) +
    ':' +
    (l ? addZero(t.seconds) : t.seconds)
  )
}
