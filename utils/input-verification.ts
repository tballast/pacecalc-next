/**
 *
 * @param e The event that needs to nbe filtered
 * @returns either the value, or "0" if its all messed up.
 */
export const verifyInput = (e: any, def = undefined) => {
  // let v = def

  // if (
  //   e.target.value === '' ||
  //   e.target.value === undefined ||
  //   e.target.value === null ||
  //   e.target.value === NaN
  // ) {
  //   return v
  // } else v = e.target.value
  // return v
  return e.target && e.target.value ? parseInt(e.target.value) : def
}
