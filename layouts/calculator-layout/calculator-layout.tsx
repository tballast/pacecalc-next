import React from 'react'
import { motion } from 'framer-motion'
import { twMerge } from 'tailwind-merge'

interface ICalculatorLayoutProps {
  children?: React.ReactNode
  style?: string
  delay?: number
}

const CalculatorLayout: React.FC<ICalculatorLayoutProps> = ({
  children,
  style,
  delay,
}) => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.5, delay: delay || 0 }} // Add a delay for one of them
      className={twMerge(
        'm-1 flex min-w-[300px] max-w-3xl flex-col rounded p-1 drop-shadow-xl md:p-4',
        style
      )}
    >
      {children}
    </motion.div>
  )
}

export default CalculatorLayout
