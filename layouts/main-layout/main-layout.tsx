import { Footer, Header, HtmlHead } from '../../components'

interface IMainLayoutProps {
  children?: React.ReactNode
  style?: string
}

const MainLayout: React.FC<IMainLayoutProps> = ({ children, style }) => {
  return (
    <>
      <HtmlHead />
      <div
        className='grid min-h-screen grid-rows-layout'
        // style={{ gridTemplateRows: 'auto 1fr auto' }}
      >
        <Header />
        <main className='box-border flex flex-col items-center'>
          {children}
        </main>
        <Footer />
      </div>
    </>
  )
}

export default MainLayout
