/**
 * Distances in Meters
 * K Prefix = Kilometers
 * M Prefix = Miles
 */
export enum DISTANCES {
  K1 = 1000,
  M1 = 1609.34,
  K5 = 5000,
  M5 = 8046.72,
  K10 = 10000,
  M10 = 16093.4,
  K15 = 15000,
  KHM = 21097.5,
  KM = 42195,
}

export enum UNITS_DISTANCE {
  MILE,
  KILOMETER,
  METER,
}

/**
 * These belong in all tables, regardless of Unit
 */
export const TABLE_STANDARD = [
  { distance: 21097.5, label: 'Half Marathon', unit: UNITS_DISTANCE.METER },
  { distance: 42195, label: 'Marathon', unit: UNITS_DISTANCE.METER },
  { distance: 16093.4, label: '10mi', unit: UNITS_DISTANCE.METER },
  { distance: 10000, label: '10k', unit: UNITS_DISTANCE.METER },
  { distance: 5000, label: '5k', unit: UNITS_DISTANCE.METER },
  { distance: 8046.72, label: '5mi', unit: UNITS_DISTANCE.METER },
]

/**
 * Table Definitions
 *
 * KM Table
 */

export const KM_TABLE = [
  { distance: 1000, label: '1k', unit: UNITS_DISTANCE.METER },
  { distance: 2000, label: '2k', unit: UNITS_DISTANCE.METER },
  { distance: 3000, label: '3k', unit: UNITS_DISTANCE.METER },
  { distance: 4000, label: '4k', unit: UNITS_DISTANCE.METER },
  { distance: 5000, label: '5k', unit: UNITS_DISTANCE.METER },
  { distance: 6000, label: '6k', unit: UNITS_DISTANCE.METER },
  { distance: 7000, label: '7k', unit: UNITS_DISTANCE.METER },
  { distance: 8000, label: '8k', unit: UNITS_DISTANCE.METER },
  { distance: 9000, label: '9k', unit: UNITS_DISTANCE.METER },
  { distance: 10000, label: '10k', unit: UNITS_DISTANCE.METER },
  { distance: 11000, label: '11k', unit: UNITS_DISTANCE.METER },
  { distance: 12000, label: '12k', unit: UNITS_DISTANCE.METER },
  { distance: 13000, label: '13k', unit: UNITS_DISTANCE.METER },
  { distance: 14000, label: '14k', unit: UNITS_DISTANCE.METER },
  { distance: 15000, label: '15k', unit: UNITS_DISTANCE.METER },
  { distance: 16000, label: '16k', unit: UNITS_DISTANCE.METER },
  { distance: 17000, label: '17k', unit: UNITS_DISTANCE.METER },
  { distance: 18000, label: '18k', unit: UNITS_DISTANCE.METER },
  { distance: 19000, label: '19k', unit: UNITS_DISTANCE.METER },
  { distance: 20000, label: '20k', unit: UNITS_DISTANCE.METER },
  { distance: 21000, label: '21k', unit: UNITS_DISTANCE.METER },
  { distance: 22000, label: '22k', unit: UNITS_DISTANCE.METER },
  { distance: 23000, label: '23k', unit: UNITS_DISTANCE.METER },
  { distance: 24000, label: '24k', unit: UNITS_DISTANCE.METER },
  { distance: 25000, label: '25k', unit: UNITS_DISTANCE.METER },
  { distance: 26000, label: '26k', unit: UNITS_DISTANCE.METER },
  { distance: 27000, label: '27k', unit: UNITS_DISTANCE.METER },
  { distance: 28000, label: '28k', unit: UNITS_DISTANCE.METER },
  { distance: 29000, label: '29k', unit: UNITS_DISTANCE.METER },
  { distance: 30000, label: '30k', unit: UNITS_DISTANCE.METER },
  { distance: 31000, label: '31k', unit: UNITS_DISTANCE.METER },
  { distance: 32000, label: '32k', unit: UNITS_DISTANCE.METER },
  { distance: 33000, label: '33k', unit: UNITS_DISTANCE.METER },
  { distance: 34000, label: '34k', unit: UNITS_DISTANCE.METER },
  { distance: 35000, label: '35k', unit: UNITS_DISTANCE.METER },
  { distance: 36000, label: '36k', unit: UNITS_DISTANCE.METER },
  { distance: 37000, label: '37k', unit: UNITS_DISTANCE.METER },
  { distance: 38000, label: '38k', unit: UNITS_DISTANCE.METER },
  { distance: 39000, label: '39k', unit: UNITS_DISTANCE.METER },
  { distance: 40000, label: '40k', unit: UNITS_DISTANCE.METER },
  { distance: 41000, label: '41k', unit: UNITS_DISTANCE.METER },
  { distance: 42000, label: '42k', unit: UNITS_DISTANCE.METER },
]
