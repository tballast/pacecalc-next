const { fontFamily } = require('tailwindcss/defaultTheme')
const colors = require('tailwindcss/colors')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './app/**/*.{js,ts,jsx,tsx}',
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './layouts/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    fontFamily: {
      mono: [
        'Azeret Mono',
        // 'Anonymous Pro',
        // 'IBM Plex Mono',
        'ui-monospace',
        'Monaco',
        'Menlo',
        'SFMono-Regular',
        'Consolas',
        'Liberation Mono',
        'Courier New',
        'monospace',
      ],
      sans: ['var(--font-ptsans)', ...fontFamily.sans],
    },
    extend: {
      gridTemplateRows: {
        layout: 'auto 1fr auto',
      },
      colors: {
        // https://coolors.co/f0f6f4-2d3047-1b998b-ff9b71-e84855
        // https://coolors.co/a81815-fcf7ff-2f4858-33658a-f6ae2d
        // 'border-light': `${colors.blue[400]}/30`,
        // 'border-dark': `${colors.slate[700]}`,

        'mint-cream': '#d0e4dd',
      },
    },
  },
  plugins: [],
  darkMode: 'class',
}
