import { useId } from 'react'
import { twMerge } from 'tailwind-merge'

interface INumberInputProps {
  label?: string
  suffix?: string
  onChange: (e: any) => void
  onBlur?: (e: any) => void
  value?: number
  min?: number
  max?: number
  containerStyle?: string
}
const NumberInput = ({
  min,
  max,
  label,
  onBlur,
  onChange,
  value,
  suffix,
  containerStyle,
}: INumberInputProps) => {
  const idStr = useId()
  const cornersStyle = suffix ? 'rounded-l-md' : 'rounded-md'
  return (
    <fieldset className={twMerge(`mb-3 w-20 md:w-24`, containerStyle)}>
      {label && (
        <label
          htmlFor={idStr}
          className='mb-2 inline-block text-gray-700 dark:text-zinc-300'
        >
          {label}
        </label>
      )}

      {/* https://mambaui.com/components/input */}
      <div className='flex'>
        <input
          min={min}
          max={max}
          onBlur={onBlur}
          onChange={onChange}
          type='number'
          className={`    
          block
          w-full
          bg-white
          bg-clip-padding
          pl-3
          text-base font-normal
          text-gray-700 focus:ring-inset dark:border-blue-400
          dark:bg-slate-600
          dark:text-gray-300
          dark:focus:border-blue-300 
          ${cornersStyle} 
          m-0
          transition
          ease-in-out
          focus:border-blue-600 focus:bg-white focus:text-gray-700 focus:outline-none
          `}
          id={idStr}
          value={value}
        />
        {suffix && (
          <span className='rounded-r-md bg-slate-200 px-3 py-1.5 text-base font-medium dark:bg-slate-500'>
            {suffix}
          </span>
        )}
      </div>

      {/* <div>
        <fieldset className='w-full space-y-1 dark:text-gray-100'>
          <label htmlFor='price' className='block text-sm font-medium'>
            Total price
          </label>
          <div className='flex'>
            <input
              type='text'
              name='price'
              id='price'
              placeholder='99 999,99'
              className='flex flex-1 text-right border sm:text-sm rounded-l-md focus:ring-inset dark:border-gray-700 dark:text-gray-100 dark:bg-gray-800 focus:ring-violet-400'
            />
            <span className='flex items-center px-3 pointer-events-none sm:text-sm rounded-r-md dark:bg-gray-700'>
              €
            </span>
          </div>
        </fieldset>
      </div> */}
    </fieldset>
  )
}
export default NumberInput
