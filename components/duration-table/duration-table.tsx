import React from 'react'
import { DISTANCES, KM_TABLE, TABLE_STANDARD } from '../../constants/distance'
import { UNITS_SPEED } from '../../constants/speed'
import useUnit from '../../hooks/use-unit'

import { calcDuration, calcPace, formatSeconds } from '../../utils/pace-logic'

interface IDurationTableProps {
  totalSeconds: number
}

const headerOffsets = [-10, -5, 0, 5, 10]
const DurationTable: React.FC<IDurationTableProps> = ({ totalSeconds }) => {
  const { unit } = useUnit()

  // https://ant.design/components/grid/#components-grid-demo-useBreakpoint
  // xs is only true for small screens
  // Others are all true up to a certain size.
  const isKm = unit === 'km'

  const isXs = false
  const offsetArr = isXs ? headerOffsets.slice(1, 4) : headerOffsets

  const headerSeconds = offsetArr.map((v) => totalSeconds + v)
  const headerValues = headerSeconds.map((v) => formatSeconds(v))
  /*@Andreas, He basically creates a temporary array (ids) that he wants to use as a filter criteria. Then he uses the filter function on the data.records array and says "remove all items that do not have an ID matching one of those in the temporary array", and reassigns this to the data.records array. The key functions here are Array.filter and Array.includes. */

  // Make temp array with its from Standard, and filter the KM or MI array based on this, then concat and sort.
  // data.records = data.records.filter( i => ids.includes( i.empid ) );

  const filterArr = TABLE_STANDARD.map((v) => v.distance)
  const kmArrayNoDupes = KM_TABLE.filter((v) => !filterArr.includes(v.distance))

  const mergedArrays = kmArrayNoDupes
    .concat(TABLE_STANDARD)
    .sort((a, b) => a.distance - b.distance)

  // console.log("filterArr", filterArr);
  // console.log("kmArrayNoDupes", kmArrayNoDupes);
  // console.log("mergedArrays", mergedArrays);

  // Iterate through the offsets and calculate the pace for each one with the distances given in the row
  const renderBodyRow = (row: any) => {
    return (
      <tr key={`row-${row.label}`} className='border '>
        <td key={row.label} className='p-1'>
          {row.label}
        </td>{' '}
        {headerSeconds.map((v) => {
          return (
            <td className='p-1 text-right' key={row.label + '-' + v}>
              {formatSeconds(
                calcDuration(
                  calcPace(
                    v,
                    isKm ? DISTANCES.K1 : DISTANCES.M1,
                    UNITS_SPEED.METERS_PER_SECOND
                  ),
                  row.distance
                )
              )}
            </td>
          )
        })}
      </tr>
    )
  }

  return (
    <div className='w-full'>
      <table>
        <thead>
          <tr>
            <th></th>
            <th className='border ' colSpan={headerValues.length}>
              Paces
            </th>
          </tr>
          <tr>
            <th className='border-t border-l border-b p-1 '>{'Distance'}</th>
            {headerValues.map((v) => {
              return (
                <th
                  className='border-t border-b p-1 last:border-r'
                  key={'header-' + v}
                >
                  {`${v} ${isKm ? ' /km' : ' /mi'}`}
                </th>
              )
            })}
          </tr>
        </thead>
        <tbody>{mergedArrays.map(renderBodyRow)}</tbody>
      </table>
    </div>
  )
}

export default DurationTable
