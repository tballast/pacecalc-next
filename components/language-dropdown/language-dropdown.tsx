import { Fragment } from 'react'
import { Menu, Transition } from '@headlessui/react'
import { IconWorld } from '@tabler/icons'
import Link from 'next/link'
import { useRouter } from 'next/router'

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ')
}

export default function LanguageDropdown() {
  const { route } = useRouter()

  return (
    <Menu as='div' className='relative block text-left md:hidden'>
      <div>
        <Menu.Button className='self-middle w-full justify-center rounded-lg bg-transparent p-2 px-4 py-2 text-blue-400  dark:text-yellow-500'>
          <IconWorld className='mx-1 h-5 w-5' aria-hidden='true' />
        </Menu.Button>
      </div>
      <Transition
        as={Fragment}
        enter='transition ease-out duration-100'
        enterFrom='transform opacity-0 scale-95'
        enterTo='transform opacity-100 scale-100'
        leave='transition ease-in duration-75'
        leaveFrom='transform opacity-100 scale-100'
        leaveTo='transform opacity-0 scale-95'
      >
        <Menu.Items className='absolute left-0 z-10 mt-2 w-56 origin-bottom-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none'>
          <div className='py-1'>
            <Menu.Item>
              {({ active }) => (
                <Link
                  href={route}
                  locale={'en'}
                  className={classNames(
                    active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                    'block px-4 py-2 text-sm'
                  )}
                >
                  EN
                </Link>
              )}
            </Menu.Item>
            <Menu.Item>
              {({ active }) => (
                <Link
                  href={route}
                  locale={'de'}
                  className={classNames(
                    active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                    'block px-4 py-2 text-sm'
                  )}
                >
                  DE
                </Link>
              )}
            </Menu.Item>
            <Menu.Item>
              {({ active }) => (
                <Link
                  href={route}
                  locale={'fr'}
                  className={classNames(
                    active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                    'block px-4 py-2 text-sm'
                  )}
                >
                  FR
                </Link>
              )}
            </Menu.Item>
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  )
}
