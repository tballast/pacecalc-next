import React from 'react'
import { ColorSchemeToggle, LanguageDropdown } from '..'
import Link from 'next/link'
import { useRouter } from 'next/router'

const Header = () => {
  const { route } = useRouter()

  // https://mambaui.com/components/header
  // Add "sticky" to the header className to get all the blur stuff back
  return (
    <header className='top-0 z-20 p-4 backdrop-blur dark:bg-transparent dark:text-gray-100 md:pt-16'>
      <div className='container mx-auto grid h-16 grid-cols-3'>
        <div className='mr-auto flex items-center'>
          <LanguageDropdown />
        </div>
        <div className='col-start-2 flex items-center justify-center self-center justify-self-center'>
          <h1
            // className='hidden bg-gradient-to-r from-green-600 to-emerald-700 bg-clip-text align-middle text-3xl font-extrabold text-transparent drop-shadow-md md:block lg:text-5xl lg:leading-tight'
            className='block bg-gradient-to-r from-blue-600 to-sky-500 bg-clip-text align-middle text-3xl font-extrabold text-transparent drop-shadow-md dark:from-blue-400 dark:to-sky-300 md:block lg:text-5xl lg:leading-tight'
          >
            PaceCalc
          </h1>
        </div>

        <div className='ml-auto flex items-center justify-end'>
          <ul className='hidden items-stretch space-x-3 md:flex'>
            <li className='flex'>
              <Link
                href={route}
                locale={'en'}
                className='-mb-1 flex items-center border-b border-blue-400/30 px-4 dark:border-transparent'
              >
                {'EN'}
              </Link>
            </li>
            <li className='flex'>
              <Link
                href={route}
                locale={'de'}
                className='-mb-1 flex items-center border-b border-blue-400/30 px-4 dark:border-transparent'
              >
                {'DE'}
              </Link>
            </li>
            <li className='flex'>
              <Link
                href={route}
                locale={'fr'}
                className='-mb-1 flex items-center border-b border-blue-400/30 px-4 dark:border-transparent'
              >
                {'FR'}
              </Link>
            </li>
          </ul>
          <ColorSchemeToggle />
        </div>
      </div>
    </header>
  )
}

export default Header
