import React from 'react'
import { twMerge } from 'tailwind-merge'

interface IPaperBox {
  className?: string
  children?: React.ReactNode
}
const PaperBox: React.FC<IPaperBox> = ({ children, className }: IPaperBox) => {
  return (
    // <div
    //   className='m-2 flex min-w-[320px]
    // flex-wrap-reverse
    // rounded bg-slate-50
    // shadow-sm dark:bg-slate-700 md:min-w-[600px]'
    // >
    //   {children}
    // </div>
    <div
      className={twMerge(
        `flex 
        min-w-[120px]
        flex-wrap-reverse 
        sm:min-w-[320px] md:min-w-[350px]`,
        className
      )}
    >
      {children}
    </div>
  )
}

export default PaperBox
