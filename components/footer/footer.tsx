import React from 'react'
import { useTranslations } from 'next-intl'
import BuyMeACoffee from '../buy-me-a-coffee'

const FooterText = () => {
  const t = useTranslations('Footer')
  return (
    <h6 className='text-md flex justify-center self-center bg-gradient-to-r from-blue-600 to-sky-500 bg-clip-text align-middle font-light text-transparent drop-shadow-md dark:from-blue-400 dark:to-sky-300 sm:col-start-2'>
      {t('by')}&nbsp;
      <a
        className='underline decoration-dotted underline-offset-2'
        href='https://gitlab.com/tballast/pacecalc-next'
      >
        Tyler Ballast
      </a>
    </h6>
  )
}

const Footer = () => {
  const t = useTranslations('Footer')

  return (
    <footer
      className='z-20 border-t border-blue-400/30 p-4 dark:border-slate-700 dark:bg-transparent dark:text-gray-100'
      // style={{ gridArea: 'footer' }}
    >
      <div className='container mx-auto grid grid-cols-2 justify-center sm:grid-cols-3'>
        <FooterText />
        <div className='flex content-end justify-end '>
          <BuyMeACoffee />
        </div>
      </div>
    </footer>
  )
}

export default Footer
