import { IconSun, IconMoonStars } from '@tabler/icons'
import { useTheme } from 'next-themes'
import React from 'react'

const ColorSchemeToggle = () => {
  const [mounted, setMounted] = React.useState(false)
  const { theme, setTheme } = useTheme()

  // useEffect only runs on the client, so now we can safely show the UI
  React.useEffect(() => {
    setMounted(true)
  }, [])

  if (!mounted) {
    return null
  }

  const onClick = () => {
    setTheme(theme === 'dark' ? 'light' : 'dark')
    // toggleColorScheme()
  }
  return (
    <button
      onClick={onClick}
      className='rounded-lg bg-transparent p-2 text-blue-400 dark:text-yellow-500'
    >
      {theme === 'dark' ? <IconSun size={22} /> : <IconMoonStars size={22} />}
    </button>
  )
}
export default ColorSchemeToggle
