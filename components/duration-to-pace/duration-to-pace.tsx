import React from 'react'
import { AtAGlanceTable, NumberInput } from '..'
import { DISTANCES } from '../../constants/distance'
import { UNITS_SPEED } from '../../constants/speed'
import useUnit from '../../hooks/use-unit'
import CalculatorLayout from '../../layouts/calculator-layout'
import { useTranslations } from 'next-intl'
import { calcPace, formatSeconds, hms2s } from '../../utils/pace-logic'
import { verifyInput } from '../../utils/input-verification'

type TimeState = {
  hours: number | undefined
  minutes: number | undefined
  seconds: number | undefined
}

const DurationToPace = () => {
  const t = useTranslations('DurationToPace')
  const { unit } = useUnit()
  const [timeState, setTimeState] = React.useState<TimeState>({
    hours: 4,
    minutes: 30,
    seconds: 0,
  })

  const verifyHours = (e: any) => {
    if (
      timeState.hours === null ||
      timeState.hours === NaN ||
      timeState.hours === undefined
    )
      setTimeState({ ...timeState, hours: 0 })
  }

  const verifyMinutes = (e: any) => {
    if (
      timeState.minutes === null ||
      timeState.minutes === NaN ||
      timeState.minutes === undefined
    )
      setTimeState({ ...timeState, minutes: 0 })
  }

  const verifySeconds = (e: any) => {
    if (
      timeState.seconds === null ||
      timeState.seconds === NaN ||
      timeState.seconds === undefined
    )
      setTimeState({ ...timeState, seconds: 0 })
  }

  const setHours = (e: any) => {
    let v = verifyInput(e)
    setTimeState({ ...timeState, hours: v })
  }
  const setMinutes = (e: any) => {
    let v = verifyInput(e)
    setTimeState({ ...timeState, minutes: v })
  }
  const setSeconds = (e: any) => {
    let v = verifyInput(e)
    setTimeState({ ...timeState, seconds: v })
  }

  const totalSeconds = hms2s(
    timeState.hours,
    timeState.minutes,
    timeState.seconds
  )

  const isKm = unit === 'km'

  const speedUnit = isKm
    ? UNITS_SPEED.MINUTES_PER_KILOMETER
    : UNITS_SPEED.MINUTES_PER_MILE

  const k1PaceMins = calcPace(totalSeconds, DISTANCES.K1, speedUnit)
  const k5PaceMins = calcPace(totalSeconds, DISTANCES.K5, speedUnit)
  const k10PaceMins = calcPace(totalSeconds, DISTANCES.K10, speedUnit)
  const k15PaceMins = calcPace(totalSeconds, DISTANCES.K15, speedUnit)
  const kHmPaceMins = calcPace(totalSeconds, DISTANCES.KHM, speedUnit)
  const kMPaceMins = calcPace(totalSeconds, DISTANCES.KM, speedUnit)

  const columns = [
    { field: 'distance', title: t('distance') },
    { field: 'pace', title: t('pace') },
  ]
  const rows = [
    { distance: '1K', pace: formatSeconds(k1PaceMins * 60) },
    { distance: '5K', pace: formatSeconds(k5PaceMins * 60) },
    { distance: '10K', pace: formatSeconds(k10PaceMins * 60) },
    { distance: '15K', pace: formatSeconds(k15PaceMins * 60) },
    { distance: t('halfMarathon'), pace: formatSeconds(kHmPaceMins * 60) },
    { distance: 'Marathon', pace: formatSeconds(kMPaceMins * 60) },
  ]

  return (
    <CalculatorLayout>
      <h2 className='test-lg m-2 font-bold dark:text-zinc-300 md:text-3xl'>
        {t('title')}
      </h2>
      <p className='text-md m-2 font-normal dark:text-zinc-300 md:text-xl'>
        {t('description')}
      </p>
      <div className='m-2 flex flex-col items-center md:flex-row md:justify-around'>
        <div className='flex flex-wrap md:self-start'>
          <NumberInput
            min={0}
            onChange={setHours}
            onBlur={verifyHours}
            suffix='H'
            label={t('hours')}
            containerStyle='md:m-2 m-1'
            value={timeState.hours}
          />

          <NumberInput
            min={0}
            max={59}
            onChange={setMinutes}
            onBlur={verifyMinutes}
            label={t('minutes')}
            suffix='M'
            containerStyle='md:m-2 m-1'
            value={timeState.minutes}
          />

          <NumberInput
            min={0}
            max={59}
            onChange={setSeconds}
            onBlur={verifySeconds}
            label={t('seconds')}
            suffix='S'
            containerStyle='md:m-2 m-1'
            value={timeState.seconds}
          />
        </div>
        <AtAGlanceTable columns={columns} rows={rows} />
      </div>
    </CalculatorLayout>
  )
}

export default DurationToPace
