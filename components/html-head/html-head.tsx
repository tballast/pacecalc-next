import React from 'react'
import Head from 'next/head'

// The HTML Header, not the visible UI header.
const HtmlHead = () => {
  return (
    <Head>
      <title>Running Pace and Duration Calculator</title>
      <meta name='viewport' content='initial-scale=1.0, width=device-width' />
      <meta
        property='og:title'
        content='Running Pace and Duration Calculator'
        key='title'
      />
      <meta
        name='description'
        content='A simple pace and duration calculator for runners. This tool allows you to very easily input a pace and see how long it would take to run anything from 1 kilometer to 50 kilometers!'
      />
      <meta name='author' content='Tyler Ballast' />
      <meta
        name='keywords'
        content='marathon, pace, tempo, speed, running, jog, pace calculator, speed calculator, running calculator, marathon pace, marathon pace calculcator, running calculator, pace chart, half marathon pace calculator, half marathon pace chart, simple pace calculator, caluclator, pace caluclator'
      />
      <meta name='robots' content='all' />

      <link
        rel='apple-touch-icon'
        sizes='180x180'
        href='/favicon/apple-touch-icon.png'
      />
      <link
        rel='icon'
        type='image/png'
        sizes='32x32'
        href='/favicon/favicon-32x32.png'
      />
      <link
        rel='icon'
        type='image/png'
        sizes='16x16'
        href='/favicon/favicon-16x16.png'
      />
      <link rel='manifest' href='/favicon/site.webmanifest' />
      <link
        rel='mask-icon'
        href='/favicon/safari-pinned-tab.svg'
        color='#5bbad5'
      />
      <meta name='msapplication-TileColor' content='#da532c' />
      <meta name='theme-color' content='#ffffff' />
    </Head>
  )
}

export default HtmlHead
