import React from 'react'
import useUnit from '../../hooks/use-unit'

const UnitChange = () => {
  const { unit, setUnit } = useUnit()
  const onRadioChange = (e: any) => {
    setUnit(e.target.value)
  }

  return (
    <div className='m-2 flex justify-center'>
      <div className='inline-flex w-fit bg-gradient-to-r from-purple-500 to-pink-500'>
        <div className='m-2 '>
          <input
            className='h-8 w-12 appearance-none rounded-md border border-gray-300 bg-slate-200 checked:bg-transparent'
            // className='appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer'
            type='radio'
            name='inlineRadioOptions'
            id='inlineRadio1'
            value='km'
            onChange={onRadioChange}
            checked={unit.toLowerCase() === 'km'}
          />
          <label
            className='inline-block text-gray-800 dark:text-rose-400'
            htmlFor='inlineRadio10'
          >
            KM
          </label>
        </div>
        <div className='m-2'>
          <input
            // className='appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer'
            type='radio'
            name='inlineRadioOptions'
            id='inlineRadio2'
            value='mi'
            onChange={onRadioChange}
            checked={unit.toLowerCase() === 'mi'}
          />
          <label
            className='inline-block text-gray-800 dark:text-rose-400'
            htmlFor='inlineRadio20'
          >
            MI
          </label>
        </div>
      </div>
    </div>
  )
}

export default UnitChange
