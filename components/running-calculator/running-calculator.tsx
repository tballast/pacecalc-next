import { useEffect, useRef, useState } from 'react'
import { PaperBox } from '..'
import useUnit, { VALID_UNITS } from '../../hooks/use-unit'
import DurationToPace from '../duration-to-pace'
import PaceToDuration from '../pace-to-duration'

const UnitSelect: React.FC = () => {
  const { unit, setUnit } = useUnit()
  // A nice animated Slider
  // https://letsbuildui.dev/articles/building-a-segmented-control-component

  // https://www.seancdavis.com/posts/animated-sliding-tabs-with-react-and-tailwind/

  const [activeTabIndex, setActiveTabIndex] = useState(0)
  const onButtonClick = (id: number) => {
    setUnit(VALID_UNITS[id])
    setActiveTabIndex(id)
  }

  const [tabUnderlineWidth, setTabUnderlineWidth] = useState(0)
  const [tabUnderlineLeft, setTabUnderlineLeft] = useState(0)

  const tabsRef = useRef<HTMLButtonElement[]>([])

  useEffect(() => {
    function setTabPosition() {
      const currentTab = tabsRef.current[activeTabIndex]
      setTabUnderlineLeft(currentTab?.offsetLeft ?? 0)
      setTabUnderlineWidth(currentTab?.clientWidth ?? 0)
    }

    setTabPosition()
    window.addEventListener('resize', setTabPosition)

    return () => window.removeEventListener('resize', setTabPosition)
  }, [activeTabIndex])

  return (
    <div>
      <div className='relative'>
        <div className='flex space-x-3'>
          {['km', 'mi'].map((unit, idx) => {
            const title = unit === 'km' ? 'KM' : 'MI'

            return (
              <button
                key={idx}
                ref={(el) => {
                  if (el) tabsRef.current[idx] = el
                }}
                className=' w-20 pt-2 pb-3'
                onClick={() => onButtonClick(idx)}
              >
                {title}
              </button>
            )
          })}
        </div>
        {/* <div className='h-1 bg-gradient-to-r from-blue-600 to-sky-500'>
          <span
            className='absolute bottom-0 block h-1 bg-trasparent transition-all duration-300 dark:from-blue-400 dark:to-sky-300'
            style={{ left: tabUnderlineLeft, width: tabUnderlineWidth }}
          />
        </div> */}

        <span
          className='bg-trasparent absolute bottom-0 block h-1 bg-gradient-to-r from-blue-600 to-sky-500 transition-all duration-300 dark:from-blue-400 dark:to-sky-300'
          style={{ left: tabUnderlineLeft, width: tabUnderlineWidth }}
        />
      </div>
    </div>
  )

  // return (
  //   <div className='m-4 flex'>
  //     {['km', 'mi'].map((interval) => {
  //       const title = interval === 'km' ? 'KM' : 'MI'
  //       const className =
  //         unit === interval
  //           ? `relative w-1/2 bg-white border-gray-200 rounded-md shadow-sm py-2 text-sm font-medium text-gray-700 whitespace-nowrap focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:z-10 sm:w-auto px-8`
  //           : `ml-0.5 relative w-1/2 border border-transparent rounded-md py-2 text-sm font-medium text-gray-700 whitespace-nowrap focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:z-10 sm:w-auto px-8`

  //       return (
  //         <button
  //           key={interval}
  //           onClick={() => setUnit(interval)}
  //           type='button'
  //           className={className}
  //         >
  //           {title}
  //         </button>
  //       )
  //     })}
  //   </div>
  // )
}

// TODO Use Framer motion library to add some motion to this site.
// MOCKUP https://wireframepro.mockflow.com/editor.jsp?editor=on&publicid=M77399330c74734b83276dbb69ca614f61668761892744&perm=Create&projectid=MO8IP04RDh&spaceid=&ptitle=Wireframe&bgcolor=white&category=Mf374100e9ea8855e75b640211778008d1662598155720&pcompany=C85148bd271540440a251134afeaf6dcb&store=yes#/page/c72ccf553101441bbfc390ed98ef7ebc
const RunningCalculator = () => {
  return (
    <>
      {/* https://stackoverflow.com/questions/65694602/create-an-animated-segmented-control-with-tailwindcss */}
      <UnitSelect />

      <div className='container mx-auto flex w-full flex-col items-center justify-center gap-2 md:grid md:grid-cols-2 md:items-start'>
        <PaperBox>
          <PaceToDuration />
        </PaperBox>
        <PaperBox>
          <DurationToPace />
        </PaperBox>
      </div>
    </>
  )
}
export default RunningCalculator
