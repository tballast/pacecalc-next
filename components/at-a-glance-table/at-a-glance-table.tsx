import { twMerge } from 'tailwind-merge'

const TD = ({
  children,
  style,
}: {
  children: React.ReactNode
  style?: string
}) => {
  return (
    <td className={twMerge(`min-w-[80px] p-2 text-right font-semibold`, style)}>
      {children}
    </td>
  )
}

const TR = ({
  children,
  style,
}: {
  children: React.ReactNode
  style?: string
}) => {
  return (
    <tr
      className={twMerge(
        `odd:bg-white even:bg-gray-50 dark:odd:bg-slate-700 dark:even:bg-slate-800`,
        style
      )}
    >
      {children}
    </tr>
  )
}

type Columns = {
  field: string
  title?: string
}

type Rows = Record<string, string | number>

const AtAGlanceTable = ({
  columns,
  rows,
}: {
  columns: Columns[]
  rows: Rows[]
}) => {
  // For the header: Iterate over the columns and render the titles

  // For the body: Iterate over the rows, then internally iterate over the cols to select the headers.
  // Optimization: YOu could probably reduce the body down to only usable fields and then only iterate over it

  return (
    <table
      className={`border-separate border-spacing-0 overflow-hidden rounded-md border`}
    >
      <thead>
        <tr>
          {columns.map((v, i) => {
            const title = v.title ?? v.field
            return (
              <th key={i} className=' p-2'>
                {title}
              </th>
            )
          })}
        </tr>
      </thead>
      <tbody>
        {rows.map((row, i) => {
          return (
            <TR key={i}>
              {columns.map((col, j) => {
                if (!row[col.field]) return null
                return (
                  <TD
                    key={j}
                    // style={j === 0 ? 'text-left' : `font-normal font-mono`}
                    style={j === 0 ? 'text-left' : `font-normal`}
                  >
                    {row[col.field]}
                  </TD>
                )
              })}
            </TR>
          )
        })}
      </tbody>
    </table>
  )
}

export default AtAGlanceTable
