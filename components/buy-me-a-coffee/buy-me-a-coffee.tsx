import Image from 'next/image'

const BuyMeACoffee = () => {
  return (
    <a
      href='https://www.buymeacoffee.com/tylerballast'
      className='m-2 flex items-center rounded-lg p-2 dark:bg-gradient-to-br dark:from-slate-600 dark:hover:bg-gradient-to-tl'
    >
      <Image
        alt='Coffee Cup'
        src='/img/bmc-logo-no-background.png'
        // src={'/img/bmc-logo.svg'}
        // layout='fixed'
        width={22}
        height={22}
      />
      {/* <span style={{ marginLeft: '6px' }}>{'Buy Me A Coffee'}</span> */}
    </a>
  )
}

export default BuyMeACoffee
