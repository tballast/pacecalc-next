import React from 'react'
import { DISTANCES } from '../../constants/distance'
import { UNITS_SPEED } from '../../constants/speed'
import useUnit from '../../hooks/use-unit'
import CalculatorLayout from '../../layouts/calculator-layout'
import { useTranslations } from 'next-intl'
import {
  calcDuration,
  calcPace,
  formatSeconds,
  hms2s,
} from '../../utils/pace-logic'
import { AtAGlanceTable, NumberInput } from '..'

import { verifyInput } from '../../utils/input-verification'

type TimeState = { minutes: number | undefined; seconds: number | undefined }
const PaceToDuration = () => {
  const t = useTranslations('PaceToDuration')

  const { unit } = useUnit()
  const [timeState, setTimeState] = React.useState<TimeState>({
    minutes: 5,
    seconds: 45,
  })

  const verifyMinutes = (e: any) => {
    if (
      timeState.minutes === null ||
      timeState.minutes === NaN ||
      timeState.minutes === undefined
    )
      setTimeState({ ...timeState, minutes: 0 })
  }

  const verifySeconds = (e: any) => {
    if (
      timeState.seconds === null ||
      timeState.seconds === NaN ||
      timeState.seconds === undefined
    )
      setTimeState({ ...timeState, seconds: 0 })
  }
  const setMinutes = (e: any) => {
    let v = verifyInput(e)
    setTimeState({ ...timeState, minutes: v })
  }
  const setSeconds = (e: any) => {
    let v = verifyInput(e)
    setTimeState({ ...timeState, seconds: v })
  }

  const totalSeconds = hms2s(0, timeState.minutes, timeState.seconds)

  const isKm = unit === 'km'
  const speed = calcPace(
    totalSeconds,
    isKm ? DISTANCES.K1 : DISTANCES.M1,
    UNITS_SPEED.METERS_PER_SECOND
  )

  const k1Finish = calcDuration(speed, DISTANCES.K1)
  const k5Finish = calcDuration(speed, DISTANCES.K5)
  const k10Finish = calcDuration(speed, DISTANCES.K10)
  const k15Finish = calcDuration(speed, DISTANCES.K15)
  const kHmFinish = calcDuration(speed, DISTANCES.KHM)
  const kMFinish = calcDuration(speed, DISTANCES.KM)

  const columns = [
    { field: 'distance', title: t('distance') },
    { field: 'finish', title: t('finishTime') },
  ]
  const rows = [
    { distance: '1K', finish: formatSeconds(k1Finish) },
    { distance: '5K', finish: formatSeconds(k5Finish) },
    { distance: '10K', finish: formatSeconds(k10Finish) },
    { distance: '15K', finish: formatSeconds(k15Finish) },
    { distance: t('halfMarathon'), finish: formatSeconds(kHmFinish) },
    { distance: 'Marathon', finish: formatSeconds(kMFinish) },
    // { distance: distance + 'km', finish: formatSeconds(kCustomFinish) },
  ]

  return (
    <CalculatorLayout style={'w-full'}>
      <h2 className='m-2 text-lg font-bold dark:text-zinc-300 md:text-3xl'>
        {t('title')}
      </h2>
      <p className='text-md m-2 font-normal dark:text-zinc-300 md:text-xl'>
        {t('description')}
      </p>
      <div className='m-2 flex flex-col flex-wrap items-center md:flex-row md:justify-around'>
        <div className='px-2 md:self-start'>
          <div className='ml-2 font-normal text-gray-700 dark:text-zinc-300'>
            {t('enterPace')}
          </div>
          <div className='flex items-center'>
            <NumberInput
              min={0}
              value={timeState.minutes}
              onChange={setMinutes}
              onBlur={verifyMinutes}
              suffix='M'
              containerStyle='m-2'
            />
            <NumberInput
              min={0}
              max={59}
              value={timeState.seconds}
              onBlur={verifySeconds}
              onChange={setSeconds}
              suffix='S'
              containerStyle='m-2'
            />

            <span className='w-7'> /{unit}</span>
          </div>
        </div>
        <AtAGlanceTable columns={columns} rows={rows} />
      </div>
    </CalculatorLayout>
  )
}

export default PaceToDuration
