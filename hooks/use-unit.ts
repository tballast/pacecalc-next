import React from 'react'
import { useQueryParam, withDefault, StringParam } from 'use-query-params'

export const VALID_UNITS = ['km', 'mi']

const useUnit = () => {
  const tz = Intl.DateTimeFormat().resolvedOptions().timeZone
  const defaultUnit = tz.includes('America/') ? 'mi' : 'km'
  const [unit, setUnit] = useQueryParam(
    'unit',
    withDefault(StringParam, defaultUnit)
  )

  // Enforce Units
  React.useEffect(() => {
    if (!VALID_UNITS.includes(unit)) {
      console.log('Invalid Unit, switching to default')
      setUnit(defaultUnit)
    }
  }, [unit])

  return { unit, setUnit }
}
export default useUnit
